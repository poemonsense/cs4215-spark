\documentclass[xcolor=dvipsnames]{beamer}

\usetheme[compress]{Singapore}

\definecolor{nus-orange}{RGB}{239,124,0} 
\definecolor{nus-blue}{RGB}{0,61,124}
\definecolor{nus-black}{RGB}{0,0,0}

\setbeamercolor{alerted text}{fg=nus-orange}
\setbeamercolor{block title}{fg=nus-blue}
\setbeamercolor{block body}{fg=nus-black}

\setbeamertemplate{theorems}[numbered]
\setbeamertemplate{propositions}[numbered]

\setbeamertemplate{bibliography item}{\insertbiblabel}

\setbeamertemplate{title page}[default][colsep=-4bp,rounded=true, shadow=true]

\setbeamerfont{footnote}{size=\tiny}

\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

\usepackage{listings}

\title{Apache Spark: Large Scale Data Processing}

\subtitle{A Designer's Perspective}

\author{Xu Yinan, A0179919E}

\institute[National University of Singapore] % (optional, but mostly needed)
{
  National University of Singapore
}

\titlegraphic{
   \includegraphics[width=2cm]{nus-logo}
}

\date{\today}

% Uncomment this, if you want the table of contents to pop up at
% the beginning of each subsection:
% \AtBeginSubsection[]
% {
%   \begin{frame}<beamer>{Outline}
%     \tableofcontents[currentsection,currentsubsection]
%   \end{frame}
% }

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\section{Background}

\begin{frame}{The Era of Data}
According to Google, ``Hot Research Topics in CS'' are ...
\pause
\begin{itemize}
    \item Machine Learning
    \item Artificial Intelligence
    \item Deep Learning
    \item Natural Language Processing
    \item Wireless Sensor Network
    \item Internet of Things (IoT)
    \item Big Data Hadoop
    \item Data Mining
    \item ...
\end{itemize}
\end{frame}

\begin{frame}{What is Apache Spark?}
  \begin{itemize}
  \item A fast and general engine for large-scale data processing\footnote[frame]{https://spark.apache.org/}
  \item Originally developed at UC Berkeley in 2009
  \pause
  \item Speed
   \begin{itemize}
       \item up to 100x faster than Hadoop MapReduce in memory, or 10x faster on disk
   \end{itemize}
  \item Ease of Use
  \begin{itemize}
      \item offers over 80 high-level operators that make it easy to build parallel apps
      \item can be used interactively from the Scala, Python and R shells
  \end{itemize}
    \item Generality
    \begin{itemize}
        \item combines SQL, streaming, and complex analytics
    \end{itemize}
    \item Runs Everywhere
  \end{itemize}
\end{frame}

\begin{frame}{Why Scala}
  \begin{quote}
  When we started Spark, we wanted it to ...\footnote[frame]{Matei Zaharia. Why is Apache Spark implemented in Scala?. Retrieved from https://www.quora.com/Why-is-Apache-Spark-implemented-in-Scala}
  \begin{itemize}
      \item have a concise API for users
      \item be fast (to work on large datasets)
      \begin{itemize}
          \item many scripting languages didn't fit the bill
          \item Scala can be quite fast because it's statically typed and it compiles in a known way to the JVM
      \end{itemize}
  \end{itemize}
    And
  \begin{itemize}
    \item running on the JVM also let us call into other Java-based big data systems, such as Cassandra, HDFS and HBase
  \end{itemize}
\end{quote}
\hspace*{0pt}\hfill Matei Zaharia

\hspace*{0pt}\hfill Creator of Apache Spark
\end{frame}

\section{Framework}
\subsection{Architecture}

\begin{frame}{Framework}
\begin{figure}
    \includegraphics[width=0.8\linewidth]{framework}
    \caption{Apache Spark Ecosystem\footnote[frame]{Scott, Jim (16 Mar. 2018.). Stream Processing Everywhere – What to Use?. Retrieved from https://mapr.com/blog/stream-processing-everywhere-what-use/}}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Components}
Every Spark application starts from creating \verb|SparkContext|\footnote[frame]{Jacek Laskowski. Anatomy of Spark Application. Retrieved from https://jaceklaskowski.gitbooks.io/mastering-apache-spark/content/spark-anatomy-spark-application.html}.
\begin{figure}
    \includegraphics[width=0.6\linewidth]{arch}
    \caption{Apache Spark Components\footnote[frame]{Jerrylead. JerryLead/SparkInternals. \textit{GitHub. }Retrieved from https://github.com/JerryLead/SparkInternals}}
\end{figure}
\end{frame}

\subsection{Basics}
\begin{frame}{Resilient Distributed Datasets (RDD)}
\begin{itemize}
    \item a fundamental data structure of Spark
    \item an RDD is a read-only, partitioned collection of elements 
\end{itemize}
\end{frame}

\begin{frame}[fragile]{RDD Operations}
RDDs support two types of operations
  \begin{itemize}
  \item Transformations
  \begin{itemize}
      \item create a new dataset from an existing one
  \end{itemize}
  \item Actions
  \begin{itemize}
      \item return a value to the driver program after running a computation on the dataset
  \end{itemize}
  \end{itemize}
\pause
All transformations in Spark are lazy
\begin{itemize}
    \item just remember the transformations applied to some base dataset
    \item only computed when an action requires a result to be returned to the driver program
\end{itemize}
\end{frame}

\begin{frame}[fragile]{RDD Operations}
For example\footnote[frame]{RDD Programming Guide. Retrieved from https://spark.apache.org/docs/latest/rdd-programming-guide.html}, 
\begin{itemize}
    \item \verb|map|
    \begin{itemize}
        \item a transformation that passes each dataset element through a function and returns a new RDD representing the results
    \end{itemize}
    \item \verb|reduce|
    \begin{itemize}
        \item an action that aggregates all the elements of the RDD using some function and returns the final result to the driver program
    \end{itemize}
\end{itemize}

\lstset{language=scala,
    basicstyle=\fontsize{9}{11}\selectfont}
\begin{lstlisting}
val lines = sc.textFile("data.txt")
val lineLengths = lines.map(s => s.length)
val totalLength = lineLengths.reduce((a, b) => a + b)
\end{lstlisting}
\end{frame}

\section{Details}
\subsection{Execution Model}

\begin{frame}[fragile]{Execution Model}
Apache Spark Architecture is based on two main abstractions

\begin{itemize}
    \item Resilient Distributed Datasets (RDD)
    \item Directed Acyclic Graph (DAG)
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Job Execution}
\begin{figure}
    \includegraphics[width=0.77\linewidth]{internals-of-job-execution-in-apache-spark}
    \caption{Internals of Job Execution in Apache Spark\footnote[frame]{Directed Acyclic Graph (DAG) in Apache Spark. \textit{DataFlair. }Retrieved from https://data-flair.training/blogs/dag-in-apache-spark/}}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Execution Model}
\begin{figure}
    \includegraphics[width=0.9\linewidth]{GeneralLogicalPlan}
    \caption{General Logical Plan\footnote[frame]{All following materials  are based on JerryLead/SparkInternals unless otherwise stated. Retrieved from https://github.com/JerryLead/SparkInternals}}
\end{figure}
\end{frame}

\begin{frame}[fragile]{RDD Dependencies}
There are two kinds of data partition dependencies between RDDs

\begin{itemize}
    \item \verb|NarrowDependency|
    \begin{itemize}
        \item Each partition of the parent RDD is used by \alert{at most one partition of the child RDD}
    \end{itemize}
    \item \verb|ShuffleDependency| 
    \begin{itemize}
        \item Each partition of the parent RDD may be used by \alert{multiple child partitions}
    \end{itemize}
\end{itemize}
\begin{figure}
    \includegraphics[width=0.75\linewidth]{dependencies}
    \caption{Narrow Dependencies vs. Wide Dependencies\footnote[frame]{Rohgar. rohgar/scala-spark-4. Retrieved from https://github.com/rohgar/scala-spark-4}}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Execution}
\begin{figure}
    \includegraphics[width=0.8\linewidth]{ComplexJobStage}
    \caption{Execution Stages of a Complex Job}
\end{figure}
\end{frame}
\end{document}