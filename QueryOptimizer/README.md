# Catalyst: Extensible Query Optimizer

Quite simple to add optimization rules to Catalyst

## Execution Time

7.2GB data (using the `lineitem` table from TPC-H)

- Original: 70.061 seconds
- UDF: 71.854 seconds
- Optimized UDF: 70.426 seconds