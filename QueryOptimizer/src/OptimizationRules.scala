package com.github.poemonsense.spark

import org.apache.spark.sql.catalyst.expressions.{Add, Literal, Multiply, ScalaUDF}
import org.apache.spark.sql.catalyst.plans.logical.LogicalPlan
import org.apache.spark.sql.catalyst.rules.Rule
import org.apache.spark.sql.types.DoubleType

object OptimizationRules {

  object LinearUDFRule extends Rule[LogicalPlan] {
    def apply(plan: LogicalPlan): LogicalPlan = {
      plan transformAllExpressions {
        case node@ScalaUDF(f, DoubleType, children, Seq(DoubleType), udfName, nullable, udfDeterministic) =>
          udfName match {
            case Some(name) if name.startsWith("udfLinearOpt") => {
              children.size match {
                case 0 => extractDouble(node.eval()) match {
                  case (true, x) => Literal(x)
                  case (false, _) => node
                }
                case 1 => {
                  val f1 = extractDouble(node.function.asInstanceOf[(Any) => Any](1.0))
                  val f0 = extractDouble(node.function.asInstanceOf[(Any) => Any](0.0))
                  if (f1._1 && f0._1) {
                    val k = f1._2 - f0._2
                    val m = f0._2
                    if (m == 0.0) {
                      Multiply(Literal(k), children.head)
                    }
                    else {
                      Add(Multiply(Literal(k), children.head), Literal(m))
                    }
                  }
                  else {
                    node
                  }
                }
                case _ => node
              }
            }
            case _ => node
          }
      }
    }
  }

  def extractDouble(x: Any): (Boolean, Double) = x match {
    case i : Double => (true, i)
    case _ => (false, 0)
  }

  def collect(): Seq[Rule[LogicalPlan]] = {
    Seq(LinearUDFRule)
  }
}
