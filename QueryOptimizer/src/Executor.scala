package com.github.poemonsense.spark

import com.github.poemonsense.spark.Schema._
import org.apache.spark.sql.SparkSession
import org.apache.log4j.Logger
import org.apache.log4j.Level

object Executor {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    val spark = SparkSession
      .builder()
      .appName("Spark SQL basic example")
      .master("local[*]")
      .getOrCreate()

    // load data, note that it's lazily evaluated
    loadData(spark, "C:/Users/xu/Downloads/data")

    // original query execution
    val t0 = System.nanoTime()
    // execute the following queries
    // other queries need further processed for evaluation in Spark
    val query1 = spark.sql("select * from lineitem where l_extendedprice < 50000")
    // query1.explain(true)
    println(query1.count())
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) / 1000000000.0 + " s")
  
    // register the udf whose name starts with udfLinearOpt
    spark.sqlContext.udf.register("udfLinearOptTest", (x: Double) => 2.0 * x + 1.0)

    // use the udf without optimization
    val t2 = System.nanoTime()
    val query2 = spark.sqlContext.sql("select * from lineitem where udfLinearOptTest(l_extendedprice) < 100001.0")
    // query2.explain(true)
    println(query2.count())
    val t3 = System.nanoTime()
    println("Elapsed time: " + (t3 - t2) / 1000000000.0 + " s")

    // add rules to Catalyst
    spark.experimental.extraOptimizations = OptimizationRules.collect()

    // use optimized udf
    val t4 = System.nanoTime()
    val query3 = spark.sqlContext.sql("select * from lineitem where udfLinearOptTest(l_extendedprice) < 100001.0")
    // query3.explain(true)
    println(query3.count())
    val t5 = System.nanoTime()
    println("Elapsed time: " + (t5 - t4) / 1000000000.0 + " s")
  }

  def loadData(spark: SparkSession, path: String): Unit = {
    def base(line: String) = line.split('|')
    def lineitemParser(line: String) = base(line) match {
      case Array(orderkey, partkey, suppkey, linenumber, quantity, extendedprice,
      discount, tax, returnflag, linestatus, shipdate, commitdate, receiptdate,
      shipinstruct, shipmode, comment) =>
        Some(Lineitem(orderkey.toInt, partkey.toInt, suppkey.toInt, linenumber.toInt,
          quantity.toDouble, extendedprice.toDouble, discount.toDouble, tax.toDouble,
          returnflag, linestatus, shipdate, commitdate, receiptdate, shipinstruct,
          shipmode, comment))
      case _ => None
    }
    import spark.implicits._
    spark.sparkContext.textFile(path.concat("/lineitem.tbl"))
      .flatMap(line => lineitemParser(line)).toDS()
      .createOrReplaceTempView("lineitem")
  }
}
