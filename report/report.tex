%!TEX program = xelatex
\documentclass[a4paper, 10pt, twocolumn]{article}

%\usepackage{graphics} % for pdf, bitmapped graphics files
%\usepackage{epsfig} % for postscript graphics files
\usepackage{amsmath} % assumes amsmath package installed
\usepackage{amssymb}  % assumes amsmath package installed\\
\usepackage{geometry}
\usepackage{fontspec}
\usepackage{listings}
\usepackage{booktabs}
\usepackage{hyperref}

\setmainfont{Times New Roman}

\title{Apache Spark: What Makes It Great\\ \large A Designer's Perspective}
\author{Xu Yinan, A0179919E}

\geometry{left=2cm,right=2cm,top=2.5cm,bottom=2.5cm}

\begin{document}

    \maketitle
    
    \section{Introduction}
    Through the mini-project of CS4215, I've dived into the design of Apache Spark and its components. Both the architecture of Spark framework and the Scala language it's built on have made Spark the greatest and most popular big data processing framework in the past few years. In this report, I explain  my thoughts in detail on the reason of Spark's being the powerful and concise framework and give three simple examples I've implemented using Spark. Due to some reasons, my research is mainly about Spark's design on Scala instead of coding. 
    
    \section{Design of Apache Spark}\label{design}
    Spark is a fast and general engine for large-scale data processing. To be specialized in this area, Spark has achieved both generality and efficiency. Both the data representation and design of the data processing workflow  have contributed much to its popularity. 
    
    \subsection{Data Structures}
    The fundamental design of a data-related system is how the data is stored in momory or on the disk. Spark has adopted two main  abstractions of data. 
    
    Since its release, Spark has been using the resilient distributed datasets (RDDs). RDD is a distributed, read-only, partitioned collection of records. Its abstraction includes partitions, dependencies, a function for computing and metadata about the data placement\cite{Zaharia:2012:RDD:2228298.2228301}. Using  this abstraction, Spark defines a set of APIs manipulating the data by firstly logging the information it needed to construct a dataset, which also provides the capability of fault-tolerance. 
    
    DataFrame is the other logical data abstraction in Spark, which was first introduced in Spark 1.3 in 2015. In this abstraction, data is organized into named columns and thus conceptually equivalent to a table in a relational database system\cite{Armbrust:2015:SSR:2723372.2742797}. DataFrame is simply a higher-level abstraction, since actually the data is stored in RDDs physically. 
    
    The introduced named columns have brought much convenience and efficiency to Spark. With the names, the user can use SQL queries to do multiple operations at a time and never needs to worry about how the partial results are organized. Besides, since the user doesn't specify how the result is produced, query optimizer can maximize its performance by optimizing the whole plan. 
    
    \subsection{Workflow}
    Apache Spark has adopted the MapReduce pattern for data processing, that is, using transformations, like \verb|map|, \verb|reduce|, etc, to produce new datasets. 
    
    In Spark, there're two types of operations, i.e., transformations and actions. Transformations are lazily evaluated and Spark will only record it until it encounters one action, which is to return some values for output to driver program. 
    
    Usually one action is after a series of transformations. To optimize the execution, Spark uses a directed acyclic graph (DAG) to analyze the logical plan. Every execution will be cut into several stages and within every stage, the transformation will be pipelined\cite{sparkinternals_2018}. 
    
    The one feature I would like to highlight is the Catalyst query optimizer in Spark SQL with good extensibility. Spark SQL integrate SQL with Spark and the user can use standard expressions in Scala to query the data. 
    
    Originally, Catalyst has both rule-based and cost-based built-in optimization. The rule-based optimization includes predicate pushdown, boolean simplification, column pruning, etc, while the cost-based optimization is mostly for \verb|join| operator. Based on the size of the table or different kinds of table, Catalyst will choose the best type of \verb|join| in \verb|HashJoin|, \verb|SortMergeJoin|, \verb|BroadcastJoin|. Although this seems to be complex, I think it is the same as optimizers in other relational database systems, where the operator is dynamically built and then optimized. 
    
    The most sparkling feature of Catalyst is its extensibility. GraphFrames is an integrated API for mixing graph and relational queries\cite{Dave:2016:GIA:2960414.2960416} that is built on Spark. To overcome the complexity of data structures in graph theory, the developers have built extensions to Catalyst to allow specific optimizations in the logical plan. Since Spark is written in Scala and also used in Scala, extensions become much easier to built than those in other framework. More detailed examples are described in Section \ref{extension_to_catalyst}. 
    
    \section{How Scala Helps Spark}
    Apache Spark is built in Scala, which is a strongly-typed functional programming language. We know that a functional programming language is very suitable for building compiler, but for a data processing framework, we know few until the rise of Spark. As the developer noted, they chose Scala mainly because of its consiceness and efficiency\cite{Zaharia:2012:RDD:2228298.2228301}. However, there're more features that have significant helped developers to both implement Spark and write examples in Spark. 
    
    \subsection{User-defined Functions}
    In most transformations and actions, the operator would accept a function argument. In Scala, there's no need that define an entire function with function name, arguments and the body. Instead, we can simply use the anonymous functions to express an inline function, as shown below. Spark will directly compile the function into JVM bytecode. 
    
    \lstset{language=scala,showstringspaces=false,basicstyle=\footnotesize\ttfamily}
    \begin{lstlisting}
val lines = sc.textFile("data.txt")
val lineLengths = lines.map(s => s.length)
val totalLength = lineLengths
  .reduce((a, b) => a + b)
    \end{lstlisting}
    
    \subsection{Pattern-matching}
    Pattern-matching is used almost everywhere in Spark, especially in the rule-based optimizer implementation, where the system needs to determine the type of a node in the logical plan, as shown below. Also, since pattern-matching is done by comparing recursively, it's quite useful when we need to match a tree-like structure. 
    \begin{lstlisting}
object BooleanSimplification 
    extends Rule[LogicalPlan]
    with PredicateHelper {
  // ......
  case a And (b Or c) 
    if Not(a).semanticEquals(b) => And(a, c)
  case a And (b Or c) 
    if Not(a).semanticEquals(c) => And(a, b)
  // ......
}
    \end{lstlisting}
        
    \subsection{Abstract Syntax Trees}
    Some SQL operators in Spark like \verb|select|, \verb|where|, \verb|join|, take in a argument that expresses a condition. Typically, to express a condition, one needs a pre-defined customized data structure, which usually comes with complex syntax. Also in Java, the user needs to use methods to access the condition class, making the code lengthy and difficult to read. However, Scala provides a feature that can capture the structure of an expression and pass it to the function as a abstract syntax tree (AST). As shown below, one can use the overloaded operators like \verb|===|, \verb|>|, \verb|+|, to concisely express conditions. 
    \begin{lstlisting}
people
  .filter(people("age") > 30)
  .join(department, 
        people("deptId") === department("id"))
  .groupBy(department("name"), people("gender"))
  .agg(avg(people("salary")), max(people("age")))
  \end{lstlisting}
    
    \subsection{Quasiquotes}
    Quasiquotes are a neat notation for manipulating Scala syntax trees with ease\cite{scala_documentation_2018} and quite like string formatting. Every string with a prefix \verb|q| would become a tree representing a given snippet. It can be used for pattern matching directly. Also, unquoting is supported using \verb|$|. In Spark, Quasiquotes are used in code generation. As shown from the example below, it's quite easy to map a variable to its value or use the variable to do some operations at runtime. Besides, quasiquotes are type-checked at compile time, which would avoid lots of errors and ensure the stability of complex systems like Spark.  
        \begin{lstlisting}
def compile(node: Node): AST = node match {
  case Literal(value) => q"$value"
  case Attribute(name) => q"row.get($name)"
  case Add(left, right) => 
    q"${compile(left)} + ${compile(right)}"
}
    \end{lstlisting}
    
    \section{Example Applications}
    \subsection{MLlib: Doing ML Easily}
    There have been a lot of machine learning libraries in the world with different features. For Spark, it also contains a machine library called MLlib that offers a set of machine learning models. Different from other frameworks, models in Spark are easily to build and the user does not need to setup the complex context in the framework. Thanks to the whole plan optimization, Spark MLlib does well in iterative algorithms. 
    
    TripleMatching is a problem in natural language processing (NLP) that is originally introduced by Prof. Luo Ping in \href{http://www.ai-fundamental.com/}{Artificical Intelligence course} in University of Chinese Academy of Sciences. We are asked to build a model that extracts correct entity triples from a given sentence, which describes the financial situation of a company in the past few years, along with the entities in it. 
    
    Typically, to build a model in MLlib, we need firstly convert the input into a \verb|vector| type after loading the data into Spark. 
    \begin{lstlisting}
val train = train_raw.withColumn("sentence",
  convertToVector($"sentence"))
    \end{lstlisting}
    
    After that, we can use the built-in multilayer perceptron classifier builder (MLPC) to setup the model. In this case, we specify the layers to be input size of \verb|params.length| with three hidden layers of size 100 and an output layer. 
    \begin{lstlisting}
val layers = Array[Int](params.length, 100, 100, 100, 2)
val classifier = new MultilayerPerceptronClassifier()
  .setLayers(layers)
  .setBlockSize(512)
  .setSeed(1234L)
  .setMaxIter(5)
  .setFeaturesCol("sentence")
  .setLabelCol("label")
  .setPredictionCol("prediction")
    \end{lstlisting}
    
    Next, we need to specify the pipeline, which is a graph of transformations on data. In this case it's the only classifier. Pipelines will be optimized as one logical plan. 
    \begin{lstlisting}
val pipeline = new Pipeline()
  .setStages(Array(classifier))
val model = pipeline.fit(train)
val resultDF = model.transform(test)
    \end{lstlisting}
    
    The final step is just applying the model to test data and evaluating the results. 
    \begin{lstlisting}
val evaluator = 
  new MulticlassClassificationEvaluator()
    .setLabelCol("label")
    .setPredictionCol("prediction")
    \end{lstlisting}
    
    With the parameters covered so far, the classifier can achieve a pretty good performance with recall, precision and F1 values of 0.9269, 0.8592, 0.8919 respectively. 
    
    \subsection{Spark SQL: Structured Data Processing}
    The \href{http://www.tpc.org/tpch/}{TPC Benchmark\texttrademark H (TPC-H)} is a decision support benchmark that consists of a suite of ad-hoc queries. It is usually used to evaluate the performance of a database system implementation. What we want to do is to execute some queries in the benchmark in Spark.
    
    To load structured data into Spark, we need first specify the schema of the tables. One example of table \verb|lineitem| is shown below. 
    \begin{lstlisting}
case class Lineitem(
  l_orderkey: Int,
  l_partkey: Int,
  l_suppkey: Int,
  l_linenumber: Int,
  l_quantity: Double,
  l_extendedprice: Double,
  l_discount: Double,
  l_tax: Double,
  l_returnflag: String,
  l_linestatus: String,
  l_shipdate: String,
  l_commitdate: String,
  l_receiptdate: String,
  l_shipinstruct: String,
  l_shipmode: String,
  l_comment: String
)
    \end{lstlisting}
    
    The second step is to use pattern-matching to load the data from file, where \verb|lineitemParser| is simply a pattern-matching expression that converts an \verb|Array| into a \verb|lineitem| instance. 
    \begin{lstlisting}
spark.sparkContext
  .textFile("<path-to-tbl>/lineitem.tbl")
  .flatMap(line => lineitemParser(line))
  .toDS()
  .createOrReplaceTempView("lineitem")
\end{lstlisting}
    
    Also note that \verb|createOrReplaceTempView| method will register a dataset as a table in Spark SQL, which is very useful when we need the partial results. 
    
    After the data is loaded, we can simply read the query from the \verb|sql| file and send it to Spark for execution. 
    \begin{lstlisting}
val query = Source
  .fromFile(s"<path-to-query>/<num>.sql")
  .mkString
spark.sql(query.stripMargin).collect()
    \end{lstlisting}
    
    Thus, just with four lines we have finished the query execution and got its result. 
    
    We can also use the DataFrame API to do the manipulation, with which we need to mannually parse the query into transformations and actions and then build the operator tree. 
    
    This example is very very simple but shows the tight integration of SQL into Spark. It's not SQL execution that matters, but the combination of structured query execution with procedural data processing. Spark SQL will regard whatever the user has written as a whole plan and perform optimization, from which we can see that Spark SQL has offered the most flexible, general and powerful data processing technique to us. 

    \subsection{Extensions to Catalyst}\label{extension_to_catalyst}
    Catalyst has been designed to enable external developers to extend the optimizer and use the full programming language while still making rules easy to specify\cite{Dave:2016:GIA:2960414.2960416}. However, one and the most challenging problem with Catalyst is that when encountering a user-defined function (UDF) the optimizer itself can do nothing to optimize the computation\cite{mastering-apache-spark_2018, xin_re_2018}. UDFs have been like a blackbox to Catalyst that even if Catalyst can access to the function, but due to the limited information of the function it cannot perform optimization in general. 
    
    In the mini-project I have tried to optimize a type of UDF, that is, the linear function $y=ax+b$. To explicitly let Catalyst try to optimize it, we ensure its name to start with \verb|udfLinearOpt|. 
    
    Typically what an optimization rule in Catalyst looks like is as shown below. An optimization rule should define a method called \verb|apply| in which it recursively uses pattern-matching on the \verb|LogicalPlan| and try to optimize the root node at its insight. 
    \begin{lstlisting}
object RemoveRedundantProject 
    extends Rule[LogicalPlan] {
  def apply(plan: LogicalPlan): LogicalPlan = 
    plan transform {
      case p @ Project(_, child) 
        if p.output == child.output => child
  }
}
    \end{lstlisting}
    
    In the example, the rule is about removing redundant \verb|project| operator. Thus, when the node is an instance of \verb|Project| and its output is the same as the \verb|child| node's output, we simply remove the \verb|Project| and return \verb|child| to higher-level node. 
    
    Therefore, to optimize a user-defined linear function, we simply use pattern-matching to access to the UDF and then determine the values of $a,b$ which in the end are replaced into the node. 
    
    Our optimization rule starts with the pattern-matching: 
    \begin{lstlisting}
case node @ ScalaUDF(f, DoubleType, children, 
    Seq(DoubleType), udfName, nullable, 
    udfDeterministic) =>
  udfName match {
    case Some(name) 
      if name.startsWith("udfLinearOpt") => {
        ...
      }
    ...
  }
    \end{lstlisting}
    
    When we come to a \verb|ScalaUDF| node with a name satisfying the requirement, we try to optimize it:
    \begin{lstlisting}
children.size match {
  ...
  case 1 => {
    val f1 = extractDouble(node.function
      .asInstanceOf[(Any) => Any](1.0))
    val f0 = extractDouble(node.function
      .asInstanceOf[(Any) => Any](0.0))
    if (f1._1 && f0._1) {
      val a = f1._2 - f0._2
      val b = f0._2
      if (b == 0.0) { 
        Multiply(Literal(a), children.head)
      }
      else {
        Add(Multiply(Literal(a), children.head),
          Literal(b))
      }
    }
    else { node }
  }
  ...
}
    \end{lstlisting}
    where \verb|extractDouble| is a function written by myself that returns a tuple \verb|(succ, val)|. If the input is can be matched to a \verb|Double| then the \verb|succ| value is \verb|true| and \verb|val| is the extracted \verb|Double| value. In the case the output is not an instance of \verb|Double|, we set the boolean \verb|succ| to \verb|false| and return anything through \verb|val|. 
    
    As shown above, the optimization rule gets access to the function and computes $a,b$ with $udf(0.0),udf(1.0)$. It then replace the node with a built-in explicit math function. 
    
    Try an example using the 7.2GB \verb|lineitem| table from TPC-H benchmark. The original query is 
    \begin{lstlisting}[language=sql]
select * from lineitem where 
  l_extendedprice < 50000
    \end{lstlisting}
    and the optimized physical plan by Catalyst is
    \begin{lstlisting}
*(1) Filter (l_extendedprice#22 < 50000.0)
+- *(1) SerializeFromObject
  +- Scan ExternalRDDScan[obj#16]
    \end{lstlisting}
    
    Define a UDF with name \verb|udfLinearOptTest| that accepts an \verb|x| of type \verb|Double| and returns \verb|2.0 * x + 1.0|. We then use it in the UDF version of query
    \begin{lstlisting}[language=sql]
select * from lineitem where 
  udfLinearOptTest(l_extendedprice) < 100001.0
    \end{lstlisting}
    that is equivalent to the previous one, and the optimized physical plan becomes
    \begin{lstlisting}
*(1) Filter (UDF:
  udfLinearOptTest(l_extendedprice#22) < 100001.0)
+- *(1) SerializeFromObject
  +- Scan ExternalRDDScan[obj#16]
    \end{lstlisting}
    
    We register the optimization rule by 
    \begin{lstlisting}
    spark.experimental.extraOptimizations = 
    OptimizationRules.collect()
    \end{lstlisting}
    where \verb|OptimizationRules| is the object we defined previously and \verb|collect| is to return the optimization rules as an \verb|Seq| instance. 
    
    Now execute the same query with UDF and the physical plan is optimized into 
    \begin{lstlisting}
*(1) Filter (
  ((2.0 * l_extendedprice#22) + 1.0) < 100001.0)
+- *(1) SerializeFromObject
  +- Scan ExternalRDDScan[obj#16]
    \end{lstlisting}
    
    Use \verb|System.nanoTime()| to record the execution time of each query. The results are as shown in Table \ref{tab1}. 
    \begin{table}[!h]
        \centering
        \caption{Execution time of the queries} \label{tab1}
        \begin{tabular}{cccc}
            \toprule
            &Original & UDF & Optimized UDF \\
            \midrule
            Time/s& 70.061 & 71.854 & 70.426 \\
            \bottomrule
        \end{tabular}
    \end{table}
    
    There's a slight difference in the execution time in three cases. Since the query and the UDF are very simple, this difference is enough to illustrate the optimization.
    
    For further optimization to UDFs, researchers from Purdue University and Stanford University have implemented Flare\cite{2017arXiv170308219E} for Spark that can replace part of or entire Catalyst optimizer. 
    \begin{lstlisting}
def sqr(fc: FlareUDFContext) = {
  import fc._;
  (y: Rep[Int]) => y * y
}
flare.udf.register("sqr", sqr)

val df = spark.sql("select ps_availqty from partsupp
  where sqr(ps_availqty) > 100")
flare(df).show()
    \end{lstlisting}
    
    As shown above, they introduce a new augument of type \verb|FlareUDFContext| into the function where they overload operators like \verb|+|, \verb|-|, \verb|*|, \verb|/|, etc. Using the information provided by these overloaded operators, they can optimize the UDFs along with the relational operations. Performance comparison shows that Flare outperforms Spark SQL in many queries from TPC-H benchmark by more than 20$\times$. 
    

    \section{Summary}
    In this mini-project what I've done includes
    \begin{itemize}
        \item Deeply diving into Apache Spark's architecture and understanding how Spark is established
        \item Understanding in detail how the execution is planned and optimized in Spark
        \item Analyzing why Spark has been so popular in the past years and summarizing the reasons as two aspects: design of the Spark framework itself and powerful features of the Scala language 
        \item Coding two simple examples using MLlib, Spark SQL to show the ease of using these libraries
        \item Implementing an optimization rule to improve the execution performance of user-defined functions, which gives the possibility of optimizing customized UDF
        \item Reseaching on the examples built on Apache Spark by other developers
        \item Reading some source code of Spark (mostly on the Catalyst)
    \end{itemize}

    The only problem I've met in this project is that I was confused about how far we should go and how many complex things we have to implement. Also because of the lack of computer clusters, I cannot try any complex job that can fully demonstrate Spark's performance. Most of the time I was searching, reading and trying to understand the Spark framework. Coding takes approximately 8 hours only. 
    
    I want to state that I didn't realize the emphasis on coding in this project until the final presentation and seeing the teachers' expressions of doubtness. I thought this project is about exploration on why a domain-specific language, like Spark, becomes the powerful tool in that specific area. And since the course is on programming language implementation, I thought we need to analyze, from the language designer's perspective, what features should a good programming language adopted to let the users do great jobs. Besides, for coding some extensions to the system, I thought we only need to know how to make extensions and code several simple examples, instead of really implementing some more extensions, which is in fact repeating trivial designs or not to do with the general technical part of language implementation in my opinion. 
    
    So, to summary up, I want to say sorry for my misunderstanding of the project. No matter what grade I could get, I would say thank you to all the teaching staff for guiding me to exploring Apache Spark from 0 to 1. I will do the remaining steps from 1 to 100 myself in the future. 
    
    The critique on the project including findings and contributions is described in previous sections. All the source code and most of the resources I've referred to can be obtained on \url{https://bitbucket.org/poemonsense/cs4215-spark/src}.
    
    \bibliographystyle{ieeetr}
    \bibliography{references}
    
\end{document}