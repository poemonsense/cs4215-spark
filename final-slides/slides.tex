\documentclass[xcolor=dvipsnames]{beamer}

\usetheme[compress]{Singapore}

\definecolor{nus-orange}{RGB}{239,124,0} 
\definecolor{nus-blue}{RGB}{0,61,124}
\definecolor{nus-black}{RGB}{0,0,0}

\setbeamercolor{alerted text}{fg=nus-orange}
\setbeamercolor{block title}{fg=nus-blue}
\setbeamercolor{block body}{fg=nus-black}

\setbeamertemplate{theorems}[numbered]
\setbeamertemplate{propositions}[numbered]

\setbeamertemplate{bibliography item}{\insertbiblabel}

\setbeamertemplate{title page}[default][colsep=-4bp,rounded=true, shadow=true]

\setbeamerfont{footnote}{size=\tiny}
\setbeamerfont{title}{size=\fontsize{13}{15.6}}

\addtobeamertemplate{navigation symbols}{}{
    \usebeamerfont{footline}
    \usebeamercolor[fg]{footline}
    \hspace{1em}
    \insertframenumber/\inserttotalframenumber
}

\usepackage{listings}
\usepackage{textcomp}
\usepackage{ulem}

\title{Apache Spark: The Taylor Swift of Big Data Software}

\subtitle{What Makes Apache Spark}

\author{Xu Yinan, A0179919E}

\institute[National University of Singapore]
{
  National University of Singapore
}

\titlegraphic{
   \includegraphics[width=2cm]{nus-logo}
}

\date{\today}

% Uncomment this, if you want the table of contents to pop up at
% the beginning of each subsection:
% \AtBeginSubsection[]
% {
%   \begin{frame}<beamer>{Outline}
%     \tableofcontents[currentsection,currentsubsection]
%   \end{frame}
% }

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{About Apache Spark}
\begin{frame}{Apache Spark}
\begin{itemize}
    \item A fast and general engine for large-scale data processing
    \item Specialized language: general and powerful in that area
    \item Large-scale data processing: processing the data correctly and quickly
\end{itemize}
\end{frame}

\begin{frame}{Outline}
\tableofcontents
\end{frame}

\section{Design of Apache Spark}

\subsection{Data Structures}

\frame{\tableofcontents[currentsubsection]}

\begin{frame}{Resilient Distributed Datasets (RDDs)}
\begin{itemize}
    \item A distributed
    memory abstraction: Read-only, partitioned collection
    of records
    \item Allows a
    general-purpose programming language to be used at interactive
    speeds for in-memory data mining on clusters
%    \pause
%    \item Representing each RDD
%    through a common interface
%    \begin{itemize}
%        \item a set of partitions, which are atomic pieces
%        of the dataset
%        \item a set of dependencies on parent RDDs
%        \item a function for computing the dataset based on its parents
%        \item metadata about its partitioning scheme and data
%        placement
%    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{DataFrame}
\begin{itemize}
    \item Motivation: enable wider audiences beyond “Big Data” engineers to leverage the power of distributed processing\cite{xin_armbrust_liu_2018}
    \item Introduced in Spark 1.3 (2015, a year of tremendous growth\cite{xin_zaharia_wendell_2018})
    \begin{itemize}
    	\item \sout{2015: Make Apache Spark Great Again}
    \end{itemize}
    
    \item A distributed collection of data organized into named columns
    \begin{itemize}
        \item Conceptually equivalent to a table in a relational database
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{DataFrame and Spark SQL}
\begin{itemize}
    \item More convenient and more efficient than Spark's procedural
    API in many common situations
    \begin{itemize}
        \item Compute multiple aggregates in one pass using a SQL statement, which is difficult to express in traditional functional APIs
    \end{itemize}
    \item DataFrame v.s. Relational Query Languages
    \begin{itemize}
        \item Integration in a full programming language
        \item Optimizations across the whole plan when
        they run an output operation
    \end{itemize}
\end{itemize}
\end{frame}

\subsection{Workflow}
\frame{\tableofcontents[currentsubsection]}

\begin{frame}[fragile]{MapReduce Pattern with Lazy Evaluation}
\begin{itemize}
    \item Two types of operations: Transformations, Actions
    \item All transformations in Spark are lazy
    \begin{itemize}
        \item Logging the transformations used to build a
        dataset rather than the actual data
    \end{itemize}
\end{itemize}
\begin{columns}
    \column{.7\textwidth}
    \lstset{language=scala,
        basicstyle=\fontsize{8}{9.6}\selectfont}
    \begin{figure}
\begin{lstlisting}
val lines = spark.textFile("hdfs://...")
val errors = lines.filter(_.startsWith("ERROR"))
errors.filter(_.contains("HDFS"))
  .map(_.split('\t')(3))
  .collect()
\end{lstlisting}
    \caption{Return the time fields of errors mentioning HDFS as an array.}
\end{figure}
    \column{.33\textwidth}
    \begin{figure}
        \includegraphics[width=\linewidth]{LineageGraph}
        \caption{Lineage graph\cite{Zaharia:2012:RDD:2228298.2228301} for the query}
    \end{figure}
\end{columns}
\end{frame}

\begin{frame}{Execution Model}
\begin{figure}
    \includegraphics[width=0.8\linewidth]{ComplexJobStage}
    \caption{Logical plan for a complex job\cite{sparkinternals_2018}}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Catalyst: The Extensible Optimizer}
\begin{itemize}
    \item The first production-quality
    query optimizer built on a functional programming language\cite{Armbrust:2015:SSR:2723372.2742797}
    \item Support both rule-based and cost-based optimization
    \item Extensibility
    \begin{itemize}
    	\item Use standard expressions in Scala, instead of a complex domain specific language, to specify rules
    \end{itemize}
\end{itemize}
\begin{figure}
    \includegraphics[width=\linewidth]{QueryPlanning}
    \caption{Query planning in Spark SQL. Catalyst has serveral libraries that handle the four phases of query execution. }
\end{figure}
\end{frame}

\begin{frame}[fragile]{Catalyst Optimizer}
Rules defined in \texttt{sql/catalyst/optimizer/*.scala}
\begin{itemize}
    \item Rule-based Optimization
    \begin{itemize}
    \item \verb|PushDownPredicate|: Pushdown filters
    \item \verb|CombineFilters|: Combine multiple filters into one
    \item \verb|BooleanSimplification|: De Morgan's laws, etc
    \item \verb|ColumnPruning|: Prune unnecessary columns
    \item \verb|ReplaceDistinctWithAggregate|
    \item ...
    \end{itemize}
    \item Cost-based Optimization (mainly for \verb|Join|)
    \begin{itemize}
        \item Join small tables first
        \item Choose one of \verb|HashJoin|/\verb|SortMergeJoin|/\verb|BroadcastJoin| based on the size of the tables
        \item ...
    \end{itemize}
\end{itemize}
\end{frame}

%\begin{frame}{GraphFrame\cite{Dave:2016:GIA:2960414.2960416}: Acceleration by Extensions to Catalyst}
%\begin{itemize}
%    \item A graph data analyzer
%    \item Letting the system materialize multiple views of the graph and executing both iterative algorithms and pattern matching using joins
%    \item Implemented as a layer on top of Spark Catalyst
%    \item Modified Catalyst to support join elimination when allowed by the foreign key relationship between vertices and edges
%    \item Optimization rules: Only 800 lines of Scala
%\end{itemize}
%\end{frame}


\section{How Scala Helps Spark}
\frame{\tableofcontents[currentsection]}

\subsection{User-defined Functions}
\begin{frame}[fragile]{User-defined Functions (UDFs)}
UDFs (usually defined as anonymous functions) make it quite easy to implement transformation. 

\begin{figure}
\lstset{language=scala,basicstyle=\fontsize{8}{9.6}\selectfont}
\begin{lstlisting}
val lines = sc.textFile("data.txt")
val lineLengths = lines.map(s => s.length)
val totalLength = lineLengths.reduce((a, b) => a + b)
\end{lstlisting}
\caption{Use UDFs and never worry about the internal data structures. }
\end{figure}
\end{frame}

\subsection{Pattern-matching}
\begin{frame}[fragile]{Pattern-matching}
The powerful pattern-matching is used almost everywhere in Spark.
\begin{figure}
	\lstset{language=scala,showstringspaces=false,basicstyle=\fontsize{8}{9.6}\selectfont}
	\begin{lstlisting}
object BooleanSimplification extends Rule[LogicalPlan]
    with PredicateHelper {
  // ......
  case a And (b Or c) if Not(a).semanticEquals(b) => And(a, c)
  case a And (b Or c) if Not(a).semanticEquals(c) => And(a, b)
  // ......
}
    \end{lstlisting}
	\caption{Implementation of rule-based optimizer relies heavily on pattern-matching. Source: \texttt{catalyst/optimizer/expressions.scala}}
\end{figure}
\end{frame}

\subsection{Expressions and Abstract Syntax Trees}
\begin{frame}[fragile]{Expressions and Abstract Syntax Trees (ASTs)}
Some operators, including projection (\verb|select|), filter (\verb|where|), \verb|join|, aggregations (\verb|groupBy|), take \textit{expression objects} in a limited domain-specific language (DSL) that lets Spark capture the structure of the expression.
\begin{figure}
\lstset{language=scala,basicstyle=\fontsize{8}{9.6}\selectfont}
\begin{lstlisting}
...
def join(right: Dataset[_], joinExprs: Column): DataFrame = 
  join(right, joinExprs, "inner")
  
def join(right: Dataset[_], joinExprs: Column, joinType: String)
      : DataFrame = {
  ...
}
\end{lstlisting}
\caption{Multiple definitions of \texttt{join} operator, which is overloaded to accept expression auguments.}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Java vs Scala}
In Java, what you need is ...

\lstset{language=java,basicstyle=\fontsize{8}{9.6}\selectfont}
\begin{lstlisting}
    people
      .filter(people.col("age").gt(30))
      .join(department,
            people.col("deptId").equalTo(department.col("id")))
      .groupBy(department.col("name"), people.col("gender"))
      .agg(avg(people.col("salary")), max(people.col("age")));
\end{lstlisting}
\pause

But in Scala, you can do the same thing with ... \lstset{language=scala,basicstyle=\fontsize{8}{9.6}\selectfont}
\begin{lstlisting}
    people
      .filter(people("age") > 30)
      .join(department, people("deptId") === department("id"))
      .groupBy(department("name"), people("gender"))
      .agg(avg(people("salary")), max(people("age")))
\end{lstlisting}
\end{frame}

\subsection{Quasiquotes}
\begin{frame}[fragile]{Quasiquotes}
Quasiquotes are a neat notation that lets you manipulate Scala syntax trees with ease\cite{scala_documentation_2018}. 
\begin{itemize}
    \item Every \verb|q"..."| will become a tree that represents a given snippet
    \lstset{language=scala,basicstyle=\fontsize{8}{9.6}\selectfont}
    \item Match whenever the structure of a given tree is equivalent to the one you've provided as a pattern
    \begin{itemize}
        \item Simply use the powerful pattern-matching tools
    \end{itemize}
    \lstset{language=scala,showstringspaces=false,basicstyle=\fontsize{8}{9.6}\selectfont}
    \begin{lstlisting}
scala> println(q"foo + bar" equalsStructure q"foo.+(bar)")
true
    \end{lstlisting}
    \item Unquoting: structurally substitute that tree into that location
    \lstset{language=scala,showstringspaces=false,basicstyle=\fontsize{8}{9.6}\selectfont}
    \begin{lstlisting}
scala> val aquasiquote = q"a quasiquote"
aquasiquote: universe.Select = a.quasiquote
scala> val tree = q"i am { $aquasiquote }"
tree: universe.Tree = i.am(a.quasiquote)
    \end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Quasiquotes}
\begin{itemize}
    \item When implementing the compiler in OCaml, we need
    \begin{itemize}
        \item \verb|match Environ.get_val ce s with|
        \item \verb|let new_ce = enum_cenv all_vs 0 in|
        \item ...
    \end{itemize}
\pause 
    \item Now with Scala ...
    \lstset{language=scala,showstringspaces=false,basicstyle=\fontsize{8}{9.6}\selectfont}
    \begin{lstlisting}
def compile(node: Node): AST = node match {
  case Literal(value) => q"$value"
  case Attribute(name) => q"row.get($name)"
  case Add(left, right) => 
    q"${compile(left)} + ${compile(right)}"
}
    \end{lstlisting}
    \pause
    \item Quasiquotes are \alert{type-checked at compile time} to ensure that only
    appropriate ASTs or literals are substituted in
    \end{itemize}
\end{frame}

\section{Example Applications}
\frame{\tableofcontents[currentsection]}

\subsection{MLlib}
\begin{frame}[fragile]{TripleMatching}
Extract correct entity triples from a given sentence, which describes the financial situation of a company. 
\begin{itemize}
    \item Given Sentence 1:
    \begin{itemize}
    \item In 2013, 2014 and January-June 2015, the balance of the issuer’s payables was 23.06 million dollars, 6.35 million dollars, and 70.118 million dollars respectively.
    \end{itemize}

    \item And the entities:
    \begin{itemize}
    \item times: 2013, 2014, January-June 2015
    \item attributes: issuer’s payables
    \item values: 23.06 million dollars, 6.35 million dollars, 70.118 million dollars
    \end{itemize}

    \item You need to output: 
    \begin{itemize}
    \item (2013, issuer’s payables, 23.06 million dollars)
    \item (2014, issuer’s payables, 6.35 million dollars)
    \item (January-June 2015, issuer’s payables, 70.118 million dollars)
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]{TripleMatching: Easy Machine Learning Using MLlib}
MLlib offers a wide range of APIs that can be easily used to build a model. 

We use Multilayer Perceptron Classifier (MLPC) here to solve the TripleMatching problem. 
\lstset{language=scala,basicstyle=\fontsize{8}{9.6}\selectfont}
\begin{lstlisting}
  val layers = Array[Int](params.length, 100, 100, 100, 2)
  val classifier = new MultilayerPerceptronClassifier()
    .setLayers(layers)
    .setBlockSize(512)
    .setSeed(1234L)
    .setMaxIter(5)
    .setFeaturesCol("sentence")
    .setLabelCol("label")
    .setPredictionCol("prediction")
  val model = new Pipeline().setStages(Array(classifier)).fit(train)
  val resultDF = model.transform(test)
\end{lstlisting}
\end{frame}

\subsection{Spark SQL}
\begin{frame}[fragile]{Spark SQL: TPC-H Queries}
We use Apache Spark to execute TPC Benchmark\texttrademark H queries. 
\lstset{language=scala,basicstyle=\fontsize{8}{9.6}\selectfont}
\begin{lstlisting}
val queries = List(1,2,3,4,5,6,10,11,12,14,16,18,19)
queries.foreach(num => {
  val query = Source.fromFile(s"<path-to-query>/$num.sql").mkString
  spark.sql(query.stripMargin).collect()
})
\end{lstlisting}

Tight integration of SQL into Spark: 4 lines of Scala. 
\end{frame}

\subsection{Optimizer for User-define Functions}
\frame{\tableofcontents[currentsubsection]}

\begin{frame}[fragile]{The Problem with Catalyst?}
Jacek Laskowski says in his book \textit{Mastering Apache Spark}\cite{mastering-apache-spark_2018}:

\begin{quote}
\alert{Use the higher-level standard column-based functions whenever possible} before reverting to developing user-defined functions since \alert{UDFs are a blackbox} for Spark SQL and it cannot (and does not even try to) optimize them.
\end{quote}
\end{frame}

\begin{frame}[fragile]{Extensible Optimizer}
We want to simplify a subset of the udf
\begin{itemize}
    \item Linear function of double floating point
    \lstset{language=scala,basicstyle=\fontsize{8}{9.6}\selectfont}
    \begin{lstlisting}
val a = 2.0
val b = 1.0
val udfLinearOptTest = udf { (x: Double) => a * x + b }
    \end{lstlisting}
\end{itemize}
Spark has offered the API to add optimization rules to Catalyst. 
\lstset{language=scala,basicstyle=\fontsize{8}{9.6}\selectfont}
\begin{lstlisting}
// add optimization rules to Catalyst
spark.experimental.extraOptimizations = OptimizationRules.collect()
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{The \texttt{OptimizationRules} Object}
\lstset{language=scala,basicstyle=\fontsize{7}{8.4}\selectfont}
\begin{lstlisting}
case node @ ScalaUDF(f, DoubleType, children, Seq(DoubleType), udfName, 
    nullable, udfDeterministic) =>
  udfName match {
    case Some(name) if name.startsWith("udfLinearOpt") => {
      children.size match {
        case 0 => extractDouble(node.eval()) match {
          case (true, x) => Literal(x)
          case (false, _) => node
        }
        case 1 => {
          val f1 = extractDouble(node.function.asInstanceOf[(Any) => Any](1.0))
          val f0 = extractDouble(node.function.asInstanceOf[(Any) => Any](0.0))
          if (f1._1 && f0._1) {
            val a = f1._2 - f0._2
            val b = f0._2
            if (b == 0.0) { Multiply(Literal(a), children.head) }
            else { Add(Multiply(Literal(a), children.head), Literal(b)) }
          }
          else { node }
        }
        case _ => node
      }
    }
    case _ => node
  }
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Changes in the Physical Plan}
Execute two queries that have the same output. 
\begin{itemize}
    \item Original Query
    
    {\fontsize{9}{10.8}\selectfont \verb|*(1) Filter (l_extendedprice#22 < 50000.0)|
    
    \verb|+- *(1) SerializeFromObject |
    
    \verb|  +- Scan ExternalRDDScan[obj#16]|}
    
    \item Query with UDF
    
    {\fontsize{9}{10.8}\selectfont \verb|*(1) Filter (UDF:udfLinearOptTest(l_extendedprice#22) < 100001.0)|
    
    \verb|+- *(1) SerializeFromObject |
    
    \verb|  +- Scan ExternalRDDScan[obj#16]|}
    
    \item Optimized Query with UDF
    
    {\fontsize{9}{10.8}\selectfont \verb|*(1) Filter (((2.0 * l_extendedprice#22) + 1.0) < 100001.0)|
    
    \verb|+- *(1) SerializeFromObject |
    
    \verb|  +- Scan ExternalRDDScan[obj#16]|}
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Performance}
Totally 7.2GB data (using the \verb|lineitem| table from TPC-H) with result of 41,033,578 rows. 
\begin{itemize}
    \item Original: 70.061 seconds
    \item UDF: 71.854 seconds
    \item Optimized UDF: 70.426 seconds
\end{itemize}
\end{frame}

\begin{frame}[fragile]{The Problem with Catalyst}
Reynold Xin from the Apache Spark project has once said on Spark's dev mailing list\cite{xin_re_2018}:
\begin{quote}
    There are \alert{simple cases in which we can analyze} the UDFs byte code and infer what it is doing, but \alert{it is pretty difficult to do in general}.
\end{quote}
\end{frame}

\begin{frame}[fragile]{Flare\cite{2017arXiv170308219E}: Optimizing UDFs in Spark}
\begin{figure}
    \includegraphics[width=\linewidth]{FlareOverview}
    \caption{Flare system overview}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Optimizing UDFs in Spark}
\lstset{language=scala,showstringspaces=false,basicstyle=\fontsize{8}{9.6}\selectfont}
\begin{lstlisting}
  // define and register UDF
  def sqr(fc: FlareUDFContext) = {
    import fc._;
    (y: Rep[Int]) => y * y
  }
  flare.udf.register("sqr", sqr)
  
  // use UDF in query
  val df = spark.sql("select
                        ps_availqty
                      from
                        partsupp
                      where
                        sqr(ps_availqty) > 100")
  flare(df).show()
\end{lstlisting}
\begin{itemize}
    \item Overload operators (\verb|+|, \verb|-|, \verb|*|, etc) in \verb|FlareUDFContext|
    \item Optimize the UDFs along with the relational operations
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Huge Performance Improvements}
\begin{figure}
    \includegraphics[width=\linewidth]{FlarePerformance}
    \caption{Performance comparison between PostgreSQL, Apache Spark, HyPer and Flare}
\end{figure}
\end{frame}

\section*{Bibliography}

\begin{frame}{References}
\tiny
\bibliographystyle{ieeetr}
\bibliography{References}
\end{frame}

\end{document}