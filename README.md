# CS4215-Spark

NUS CS4215 Mini-project on [Apache Spark](https://spark.apache.org/) with Scala, AY2017-18 Semester 2

## Purpose of the mini-project

Do a systematic exploration in some specialized domains.

## About Apache Spark

- Key point about *specialized* language: it should be general and powerful in that particular area
- Large Scale Data Processing: focus on *processing* the *data* correctly and quickly
- Abstractions in Apache Spark
  - For the data: Resilient Distributed Datasets (RDDs)
  - For the processing: Transformations and Actions
- The Spark Programming Model
- Originally implemented in Scala for its consiceness and efficiency

## Design of Apache Spark

### RDD: Data-sharing Abstraction

A read-only, partitioned collection of records

Fault-tolerance

Spark: the Programming Interface

### DataFrame and Spark SQL

Collections of structured records that can be manipulated using Spark’s procedural API

DataFrame API can perform relational operations on both external data sources and Spark’s built-in distributed collections

### MapReduce Pattern and Lazy Evaluation

Two types of operations

- Transformations
- Actions

All transformations in Spark are lazy and only computed when an action requires a result to be returned to the driver program. 

### Directed Acyclic Graph (DAG) and Execution Model 

See [Survey Presentation Slides](survey-presentation-slides/slides.pdf)

### Catalyst and Project Tungsten

Catalyst

- a new extensible query optimizer which supports both rule-based and cost-based optimization
- the first production-quality query optimizer built on a functional programming language, which uses standard features of Scala

Project Tungsten: for Further Optimization

## How Scala Helps Spark

### User-defined Functions (UDFs)

Usually defined as anonymous function in Scala

```scala
val lines = sc.textFile("data.txt")
val lineLengths = lines.map(s => s.length)
val totalLength = lineLengths.reduce((a, b) => a + b)
```

### Expressions and Abstract Syntax Trees (ASTs)

```scala
employees
  .join(dept, employees("deptId") === dept("id"))
  .where(employees("gender") === "female")
  .groupBy(dept("id"), dept("name"))
  .agg(count("name"))
```

### Catalyst Optimizer and Tungsten

#### Pattern-matching

Pattern-matching is used almost everywhere in the rule-based optimizer. 

In `catalyst/optimizer/expressions.scala`: 

```scala
object BooleanSimplification extends Rule[LogicalPlan] with PredicateHelper {
    // ......
    case a And (b Or c) if Not(a).semanticEquals(b) => And(a, c)
    case a And (b Or c) if Not(a).semanticEquals(c) => And(a, b)
    // ......
}
```

#### Quasiquotes

```scala
def compile(node: Node): AST = node match {
  case Literal(value) => q"$value"
  case Attribute(name) => q"row.get($name)"
  case Add(left, right) => q"${compile(left)} + ${compile(right)}"
}
```

#### Extensions

[GraphFrame](references/papers/Graphframes an integrated api for mixing graph and relational queries.pdf) and [Flare](references/papers/Flare Native Compilation for Heterogeneous Workloads in Apache Spark.pdf)

## Example Applications

### TripleMatching: Easy-to-code ML Using MLlib

See [TripleMatching](TripleMatching/README.md). 

### Spark SQL: Simply Execute TPC-H Queries

See [SparkSQL-TPCH](SparkSQL-TPCH/README.md). 

### Extension to Catalyst: A Simple Optimizer for UDFs

See [QueryOptimizer](QueryOptimizer/README.md). 

## Final Presentation and Report

See [the slides](final-slides/slides.pdf) and [the report](report/report.pdf) . 