# Paper-reading

Mainly focus on Scala related features

## Spark SQL Introduction

[Spark SQL: Relational Data Processing in Spark](https://amplab.cs.berkeley.edu/wp-content/uploads/2015/03/SparkSQLSigmod2015.pdf)

- DataFrame: distributed collection of rows with homogeneous schema
  - `expression`: captured as abstract syntax tree (AST), rather than Scala functions
  - user-defined functions (UDF): use general-purpose language
- Catalyst: query optimizer
  - implementations based on functional programming constructs
  - use standard features of the Scala programming language, e.g., pattern-matching
  - several sets of rules that handle different phases of query execution
    - code generation: quasiquotes (Scala feature)
- Performance
  - Fast and concise
    - (pyspark) In DataFrame API, only the logical plan is constructed in Python, and all physical execution is compiled down into native Spark code as JVM bytecode
  - Extensible optimizer
    - build the optimizer using standard features of a functional programming language

## Flare: Improvements to Spark SQL System

[Flare: Native Compilation for Heterogeneous Workloads in Apache Spark](https://arxiv.org/abs/1703.08219)

- Some problems with native Spark SQL
  - Hegerogeneous Workloads: Query optimization techniques are ineffective if there're UDFs
  - Catalyst does not yet perform any kind of join reordering
    - users need to manually encode a good join
- Flare implementation
- Performance: TPCH benchmark

## GraphFrame: Extension to Catalyst

[GraphFrames: An Integrated API for Mixing Graph and Relational Queries](https://event.cwi.nl/grades/2016/02-Dave.pdf)

- Implemented as a layer on top of Spark Catalyst
  - taking patterns as input
  - collecting statistics using Catalyst APIs
  - emitting a Catalyst logical plan
- Modified Catalyst to support join elimination when allowed by the foreign key relationship between vertices and edges
- Main part: 800 lines of Scala



