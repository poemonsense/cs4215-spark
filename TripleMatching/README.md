# TripleMatching

Example app of Apache Spark for using Multilayer Perceptron Classifier (MLPC) to solve the TripleMatching problem

## TripleMatching Problem

The TripleMatching Problem was originally introduced in the Artificial Intelligence course, University of Chinese Academy of Sciences (UCAS), offered by Prof. [Luo Ping](http://mldm.ict.ac.cn/MLDM/luoping/). Originially, it's required to build an long short-term memory (LSTM) model instead of an MLPC. 

### Description

Build an MLPC model that extracts correct entity triples from a given sentence, which describes the financial situation of a company in the past few years, along with the entities in it. 

### Example

Given Sentence 1: `In 2013, 2014 and January-June 2015, the balance of the issuer’s payables was 23.06 million yuan, 6.35 million yuan, and 70.118 million yuan respectively.`

Extract some entities from the sentence, including **time**, **attribute**, and **value**.

- times: `2013`, `2014`, `January-June 2015`


- attributes: `issuer’s payables` (attributes might be not complete)
- values: `23.06 million yuan`, `6.35 million yuan`, `70.118 million yuan`

Define a triple consisting of three ordered components: `[time, attribute, value]`. If the sentence states that **at time `t`, the value of attribute a was `v`**, we say triple `[t, a, v]` is a ***correct triple***.

Therefore, all correct triples in Sentence 1 are: 

- `[2013, issuer’s payables, 23.06 million yuan]`
- `[2014, issuer’s payables, 6.35 million yuan]`
- `[January-June 2015, issuer’s payables, 70.118 million yuan]`

### Training Data

A json file `assignment_training_data_word_segment.json` containing a list of dict in the format

```json
[
    {
        "sentenceId": "eadf4c4d7eaa6cb767fa6d8c02555f5-eb85e9fb6ec57b2dd9ba53a8cc4b1625b18",
        "sentence": "In FY2013, FY2014, and FY2015, the company's income from financial leasing business was 58.2121 million yuan, 1043.8684 million yuan and 147579.60 million yuan respectively, which accounted for 59.21%, 66.59%, and 66.78% of the issuer's operating income.",
        "words": ["FY2013", "FY2014", "FY2015", "company", "financial leasing business", "income", "respectively", "58.2121 million yuan", "1043.8684 million yuan", "147579.60 million yuan", "accounted for", "issuer", "operating income", "59.21%", "66.59%", "66.78%"],
        "indexes": [0, 0, 0, 7, 13, 1, 11, 2, 2, 2, 17, 1, 2, 2, 2],  
        "times": [0, 1, 2],               // ["FY2013", "FY2014", "FY2015"]
        "attributes": [5, 11],            // ["income", "operating income"]
        "values": [7, 8, 9, 12, 13, 14],  // ["58.2121 million yuan", "1043.8684 million yuan", "147579.60 million yuan", "59.21%", "66.59%", "66.78%"]
        "results": [
            [0, 5, 7],   // ["FY2013", "income", "58.2121 million yuan"  ]
            [1, 5, 8],   // ["FY2014", "income", "1043.8684 million yuan"]
            [2, 5, 9]    // ["FY2015", "income", "147579.60 million yuan"]
        ]
    }
	,
    ...
]
```

## Implementations in Apache Spark

Inspired by [an article on IBM deleloperWorks](https://www.ibm.com/developerworks/cn/opensource/os-cn-spark-practice6/) and based on [MLPC API offered by Apache Spark](https://spark.apache.org/docs/latest/ml-classification-regression.html#multilayer-perceptron-classifier). 

### Usage

```bash
TripleMatching: an example app for MLPC.
Usage: TripleMatching <train> <test> <length>

  <train>   input paths to training samples
  <test>    input paths to testing samples
  <length>  length of input sentence
```

### Evaluation

Obviously MLPC doesn't work as well as LSTM. 

Using hidden layers of `100, 100, 100`, running 5 iterations, the model achieves a performance of

- Recall: 0.9269
- Precision: 0.8592
- F1: 0.8919