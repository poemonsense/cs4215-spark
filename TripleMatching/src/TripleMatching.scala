/**
  * Example app of Apache Spark for using MLPC to solve the TripleMatching problem
  *
  * For mini-project of CS4215, AY2017-18
  * Xu Yinan, National University of Singapore
  *
  */

package com.github.poemonsense.spark

import org.apache.spark
import org.apache.spark.examples.mllib.AbstractParams
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.MultilayerPerceptronClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf
import scopt.OptionParser

object TripleMatching {
  case class Params(
    train: String = "train.json",
    test: String = "test.json",
    length: Int = 154
  ) extends AbstractParams[Params]

  def main(args: Array[String]) {
    val defaultParams = Params()

    val parser = new OptionParser[Params]("TripleMatching") {
      head("TripleMatching: an example app for MLPC.")
      arg[String]("<train>")
        .required()
        .text("input paths to training samples")
        .action((x, c) => c.copy(train = x))
      arg[String]("<test>")
        .required()
        .text("input paths to testing samples")
        .action((x, c) => c.copy(test = x))
      arg[Int]("<length>")
        .required()
        .text("length of input sentence")
        .action((x, c) => c.copy(length = x))
    }
    parser.parse(args, defaultParams) match {
      case Some(params) => run(params)
      case _ => sys.exit(1)
    }
  }

  def run(params: Params): Unit = {
    val appName = s"LinearRegression with $params"
    val sqlContext = SparkSession.builder().appName(appName).master("local[*]").getOrCreate()

    // load data from file
    val (train_raw, test_raw) = load_dataset(sqlContext, params)

    // note that MLlib functions accecpt Vector[Double] as input, we need to do the transformation
    val convertToVector = udf((arr: Seq[Long]) => Vectors.dense(arr.toArray.map(_.toDouble)))
    import sqlContext.implicits._
    val train = train_raw.withColumn("sentence", convertToVector($"sentence"))
    val test = test_raw.withColumn("sentence", convertToVector($"sentence"))

    // Instantiate a MLPC of layers of params.length, 100, 100, 100, 2
    val layers = Array[Int](params.length, 100, 100, 100, 2)
    val classifier = new MultilayerPerceptronClassifier()
      .setLayers(layers)
      .setBlockSize(512)
      .setSeed(1234L)
      .setMaxIter(5)
      .setFeaturesCol("sentence")
      .setLabelCol("label")
      .setPredictionCol("prediction")

    // generate the pipelines
    val pipeline = new Pipeline().setStages(Array(classifier))
    val model = pipeline.fit(train)
    val resultDF = model.transform(test)
    // resultDF.write.json("results")

    // evaluate the model using precision, recall and F1
    val evaluator = new MulticlassClassificationEvaluator()
      .setLabelCol("label")
      .setPredictionCol("prediction")

    evaluator.setMetricName("weightedRecall")
    val recall = evaluator.evaluate(resultDF)
    evaluator.setMetricName("weightedPrecision")
    val precision = evaluator.evaluate(resultDF)
    evaluator.setMetricName("f1")
    val f1 = evaluator.evaluate(resultDF)

    println("Recall: %2.4f, Precision: %2.4f, F1: %2.4f".format(recall, precision, f1))
  }

  def load_dataset(context: SparkSession, params: Params)
      : (spark.sql.DataFrame, spark.sql.DataFrame) = {
    val train = context.read.json(params.train).toDF()
    val test = context.read.json(params.test).toDF()
    (train, test)
  }
}
