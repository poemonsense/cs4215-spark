import json
from random import shuffle

filename = "assignment_training_data_word_segment.json"
with open(filename, 'r') as file:
    data = json.load(file)
shuffle(data)

max_length = max([len(item['indexes']) for item in data])
print("Maximum sentence size: {}".format(max_length))

length = int(len(data) * 0.9)
train_data, test_data = data[:length], data[length:]

def embedding(dataset):
    positive = []
    negative = []
    for item in dataset:
        idxs = item['indexes']
        idxs = [2*x for x in idxs]
        for x in item['times']:
            for y in item['attributes']:
                for z in item['values']:
                    new_idxs = list(idxs)
                    new_idxs[x] += 1
                    new_idxs[y] += 1
                    new_idxs[z] += 1
                    if len(new_idxs) < max_length:
                        new_idxs += [-1 for i in range(max_length-len(new_idxs))]
                    if [x,y,z] in item['results']:
                        positive.append(new_idxs)
                    else:
                        negative.append(new_idxs)
    return positive, negative



train_positive, train_negative = embedding(train_data)

shuffle(train_negative)
train_negative = train_negative[:len(train_negative) // 10]
print("Training data: positive {}, negative {}".format(len(train_positive), len(train_negative)))

train = [{"label":1, "sentence":x} for x in train_positive]
train += [{"label":0, "sentence":x} for x in train_negative]
shuffle(train)
with open('train.json', 'w+') as outfile:
    json.dump(train, outfile)

test_positive, test_negative = embedding(test_data)
print("Testing data: positive {}, negative {}".format(len(test_positive), len(test_negative)))

for x in test_positive:
    try:
        assert(len(x) == max_length)
    except:
        print(x)
for x in test_negative:
    try:
        assert(len(x) == max_length)
    except:
        print(x)

test = [{"label":1, "sentence":x} for x in test_positive]
test += [{"label":0, "sentence":x} for x in test_negative]
with open('test.json', 'w+') as outfile:
    json.dump(test, outfile)
