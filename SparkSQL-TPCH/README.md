# Spark SQL: TPC-H Queries

Use Apache Spark to Run TPC-H Queries

## Ease of Use

Only 10 lines for loading data and executing the query. 

```scala
val spark = SparkSession
  .builder()
  .appName("Spark SQL basic example")
  .master("local[*]")
  .getOrCreate()

loadData(spark, "path-to-data")
val queries = List.range(1, 20, 1)
queries.foreach(num => {
  val query = Source.fromFile(s"path-to-queries/$num.sql").mkString.split(';')(0)
  spark.sql(query.stripMargin).collect()
})
```

Register any results as table using `createOrReplaceTempView("viewName")`