package com.github.poemonsense.spark

import com.github.poemonsense.spark.Schema._
import org.apache.spark.sql.SparkSession

import scala.io.Source

object Executor {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .appName("Spark SQL basic example")
      .master("local[*]")
      .getOrCreate()
    
    // uncomment following lines to enable cost-based Join optimizer
    // import org.apache.spark.sql.internal.SQLConf.{CBO_ENABLED, JOIN_REORDER_ENABLED}
    // spark.conf.set(CBO_ENABLED.key, true)
    // spark.conf.set(JOIN_REORDER_ENABLED.key, true)

    // load data, note that it's lazily evaluated
    loadData(spark, "C:/cs/cs4215-spark/SparkSQL-TPCH/data")

    val t0 = System.nanoTime()

    // execute the following queries
    // other queries need further processed for evaluation in Spark
    val queries = List(1,2,3,4,5,6,10,11,12,14,16,18,19)
    queries.foreach(num => {
      val query = Source.fromFile(s"C:/cs/cs4215-spark/SparkSQL-TPCH/queries/$num.sql").mkString.split(';')(0)
      // spark.sql(query.stripMargin).explain(true)
      spark.sql(query.stripMargin).collect()
    })

    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) / 1000000000 + " s")
  }

  def loadData(spark: SparkSession, path: String): Unit = {
    def base(line: String) = line.split('|')
    def partParser(line: String) = base(line) match {
      case Array(partkey, name, mfgr, brand, tpe, size, container,
      retailprice, comment) =>
        Some(Part(partkey.toInt, name, mfgr, brand, tpe, size.toInt,
          container, retailprice.toDouble, comment))
      case _ => None
    }
    def supplierParser(line: String) = base(line) match {
      case Array(suppkey, name, address, nationkey, phone, acctbal, comment) =>
        Some(Supplier(suppkey.toInt, name, address, nationkey.toInt, phone,
          acctbal.toDouble, comment))
      case _ => None
    }
    def partsuppParser(line: String) = base(line) match {
      case Array(partkey, suppkey, availqty, supplycost, comment) =>
        Some(Partsupp(partkey.toInt, suppkey.toInt, availqty.toInt,
          supplycost.toDouble, comment))
      case _ => None
    }
    def customerPaser(line: String) = base(line) match {
      case Array(custkey, name, address, nationkey, phone, acctBal, mktsegment, comment) =>
        Some(Customer(custkey.toInt, name, address, nationkey.toInt, phone,
          acctBal.toDouble, mktsegment, comment))
      case _ => None
    }
    def ordersParser(line:String) = base(line) match {
      case Array(orderkey, custkey, orderstatus, totalprice, orderdate, orderpriority,
      clerk, shippriority, comment) =>
        Some(Orders(orderkey.toInt, custkey.toInt, orderstatus, totalprice.toDouble,
          orderdate, orderpriority, clerk, shippriority.toInt, comment))
      case _ => None
    }
    def lineitemParser(line: String) = base(line) match {
      case Array(orderkey, partkey, suppkey, linenumber, quantity, extendedprice,
      discount, tax, returnflag, linestatus, shipdate, commitdate, receiptdate,
      shipinstruct, shipmode, comment) =>
        Some(Lineitem(orderkey.toInt, partkey.toInt, suppkey.toInt, linenumber.toInt,
          quantity.toDouble, extendedprice.toDouble, discount.toDouble, tax.toDouble,
          returnflag, linestatus, shipdate, commitdate, receiptdate, shipinstruct,
          shipmode, comment))
      case _ => None
    }
    def nationParser(line: String) = base(line) match {
      case Array(nationkey, name, regionkey, comment) =>
        Some(Nation(nationkey.toInt, name, regionkey.toInt, comment))
      case _ => None
    }
    def regionParser(line: String) = base(line) match {
      case Array(regionkey, name, comment) => Some(Region(regionkey.toInt, name, comment))
      case _ => None
    }

    import spark.implicits._
    spark.sparkContext.textFile(path.concat("/part.tbl"))
        .flatMap(line => partParser(line)).toDS()
        .createOrReplaceTempView("part")
    spark.sparkContext.textFile(path.concat("/supplier.tbl"))
      .flatMap(line => supplierParser(line)).toDS()
      .createOrReplaceTempView("supplier")
    spark.sparkContext.textFile(path.concat("/partsupp.tbl"))
      .flatMap(line => partsuppParser(line)).toDS()
      .createOrReplaceTempView("partsupp")
    spark.sparkContext.textFile(path.concat("/customer.tbl"))
      .flatMap(line => customerPaser(line)).toDS()
      .createOrReplaceTempView("customer")
    spark.sparkContext.textFile(path.concat("/orders.tbl"))
      .flatMap(line => ordersParser(line)).toDS()
      .createOrReplaceTempView("orders")
    spark.sparkContext.textFile(path.concat("/lineitem.tbl"))
      .flatMap(line => lineitemParser(line)).toDS()
      .createOrReplaceTempView("lineitem")
    spark.sparkContext.textFile(path.concat("/nation.tbl"))
      .flatMap(line => nationParser(line)).toDS()
      .createOrReplaceTempView("nation")
    spark.sparkContext.textFile(path.concat("/nation.tbl"))
      .flatMap(line => nationParser(line)).toDS()
      .createOrReplaceTempView("n1")
    spark.sparkContext.textFile(path.concat("/nation.tbl"))
      .flatMap(line => nationParser(line)).toDS()
      .createOrReplaceTempView("n2")
    spark.sparkContext.textFile(path.concat("/region.tbl"))
      .flatMap(line => regionParser(line)).toDS()
      .createOrReplaceTempView("region")
  }
}

